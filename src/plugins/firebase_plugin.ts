import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { getAuth } from 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyAuJVOMsMGp_mtUqe1taejzE5qvrkwVhug',
  authDomain: 'kanapka-240825.firebaseapp.com',
  projectId: 'kanapka-240825',
  storageBucket: 'kanapka-240825.appspot.com',
  messagingSenderId: '816063035614',
  appId: '1:816063035614:web:2e937725f66a7023feaa4f',
  measurementId: 'G-1KT88TFLLM'
}

export const firebase = initializeApp(firebaseConfig)
export const database = getFirestore(firebase)
export const auth = getAuth(firebase)
