import { ref, Ref, computed, onMounted } from 'vue'
import { defineStore } from 'pinia'
import { auth, database } from '@/plugins/firebase_plugin'
import {
  setDoc,
  deleteDoc,
  updateDoc,
  collection,
  doc,
  query,
  where,
  getDocs
} from 'firebase/firestore'

export const useTodoLogicStore = defineStore('todoLogic', () => {
  interface Todo {
    id: number
    text: string
    done: boolean
    duringEdit: boolean
  }

  let id = 0
  let index: number
  let userID = ''

  const newTodoText = ref('')
  const hideCompleted = ref(false)
  const editingTodo = ref(false)
  const editTodoText = ref('')
  const todos = ref<Todo[]>([])
  const databaseName = 'tasks'
  const currentUser = auth.currentUser

  if (currentUser) {
    userID = currentUser.uid
  }

  const TasksCollectionRef = query(
    collection(database, databaseName),
    where('UsersId', '==', userID)
  )

  const insertIntoDB = async (id: number, text: string) => {
    setDoc(doc(collection(database, databaseName)), {
      id: id,
      text: text,
      done: false,
      duringEdit: false,
      UsersId: userID
    })
  }

  const removeFromDB = (id: number) => {
    const q = query(TasksCollectionRef, where('UsersId', '==', userID), where('id', '==', id))
    getDocs(q)
      .then((querySnapshot) => {
        const currentDoc = querySnapshot.docs[0]
        const documentRef = doc(database, databaseName, currentDoc.id)

        deleteDoc(documentRef)
          .then(() => {
            console.log('Document successfully deleted!')
          })
          .catch((error) => {
            console.error('Error deleting document:', error)
          })
      })
      .catch((error) => {
        console.error('Error retrieving documents:', error)
      })
  }

  const readFromDB = async () => {
    return getDocs(TasksCollectionRef)
      .then((snapshot) => {
        const docs = snapshot.docs
        if (Array.isArray(docs)) {
          const todos: Todo[] = docs.map((item) => item.data() as Todo)
          return todos
        }
        return <Todo[]>[]
      })
      .catch((error) => {
        console.error('Error retrieving documents:', error)
        return <Todo[]>[]
      })
  }

  const editInDB = (index: number, newText: string, done: boolean) => {
    const q = query(TasksCollectionRef, where('UsersId', '==', userID), where('id', '==', index))
    getDocs(q)
      .then((querySnapshot) => {
        const currentDoc = querySnapshot.docs[0]
        const documentRef = doc(database, databaseName, currentDoc.id)
        const updateData = {
          text: newText,
          done: done
        }

        updateDoc(documentRef, updateData)
          .then(() => {
            console.log('Document successfully updated!')
          })
          .catch((error) => {
            console.error('Error updating document:', error)
          })
      })
      .catch((error) => {
        console.error('Error retrieving documents:', error)
      })
  }

  onMounted(() => {
    readFromDB()
      .then((returnedTodos) => {
        todos.value = returnedTodos
        id = returnedTodos.reduce((max, todo) => {
          return todo.id > max ? todo.id : max
        }, 0)
      })
      .catch((error) => {
        console.error('Error retrieving documents:', error)
      })
  })

  const rules: Ref<((value: string) => boolean | string)[]> = ref([
    (value: string) => {
      if (value.length > 3 || value.length === 0) return true
      return 'Zadanie musi mieć przynajmniej 4 znaki'
    }
  ])

  const filteredTodos = computed(() => {
    return hideCompleted.value ? todos.value.filter((t) => !t.done) : todos.value
  })

  function markAsDone(todo: Todo) {
    todo.done = !todo.done
    editInDB(todo.id, todo.text, todo.done)
  }

  async function addTodo() {
    if (newTodoText.value.length < 4) return

    const newTodo: Todo = {
      id: id,
      text: newTodoText.value,
      done: false,
      duringEdit: false
    }
    todos.value.push(newTodo)

    await insertIntoDB(id, newTodoText.value)

    id++
    newTodoText.value = ''
  }

  function removeTodo(todo: Todo) {
    if (todo.duringEdit) {
      return
    }

    removeFromDB(todo.id)

    todos.value = todos.value.filter((t) => t !== todo)
  }

  function sortByID() {
    return todos.value.sort((a, b) => a.id - b.id)
  }

  function sortAlphabetically() {
    return todos.value.sort((a, b) => {
      return a.text.localeCompare(b.text)
    })
  }

  function sortByDone() {
    return todos.value.sort((a, b) => {
      if (a.done === b.done) return 0
      else if (a.done) return -1
      else if (b.done) return 1
      return 0
    })
  }

  function editTodo(todo: Todo) {
    if (editingTodo.value) {
      return
    }

    editingTodo.value = !editingTodo.value
    todo.duringEdit = !todo.duringEdit

    editTodoText.value = todo.text
    index = todo.id
  }

  function zmienZadanie() {
    if (editTodoText.value.length < 4) {
      return
    }

    todos.value.forEach((todo) => {
      if (todo.id == index) {
        todo.text = editTodoText.value
        todo.duringEdit = false
        editInDB(todo.id, editTodoText.value, todo.done)
      }
    })
    editTodoText.value = ''
    editingTodo.value = false
  }

  return {
    newTodoText,
    hideCompleted,
    editingTodo,
    editTodoText,
    rules,
    filteredTodos,
    markAsDone,
    addTodo,
    removeTodo,
    sortByID,
    sortAlphabetically,
    sortByDone,
    editTodo,
    zmienZadanie
  }
})
