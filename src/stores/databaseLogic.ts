import { defineStore } from 'pinia'
import { auth } from '../plugins/firebase_plugin'
import router from '../router'
import { ref } from 'vue'
import {
  signInWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithPopup,
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  getAuth
} from 'firebase/auth'

export const useDatabaseLogic = defineStore('databaseLogic', () => {
  const isUserLogged = ref(false)

  const getCurrentUser = () => {
    return new Promise((resolve, reject) => {
      const removeListener = onAuthStateChanged(
        getAuth(),
        (user) => {
          removeListener()
          resolve(user)
        },
        reject
      )
    })
  }

  const login = (email: string, password: string) => {
    signInWithEmailAndPassword(auth, email, password)
      .then(() => {
        console.log('Successfully logged in!')
        router.push('/dashboard')
        isUserLogged.value = true
      })
      .catch((error) => {
        console.log(error.message)
        alert(error.message)
      })
  }

  const loginGoogle = () => {
    const provider = new GoogleAuthProvider()
    signInWithPopup(auth, provider)
      .then(() => {
        console.log('Successfully logged with google account!')
        router.push('/dashboard')
        isUserLogged.value = true
      })
      .catch((error) => {
        console.log(error.message)
      })
  }

  const register = (email: string, password: string) => {
    createUserWithEmailAndPassword(auth, email, password)
      .then(() => {
        console.log('Successfully registered!')
        router.push('/dashboard')
        isUserLogged.value = true
      })
      .catch((error) => {
        console.log(error.message)
        alert(error.message)
      })
  }

  const registerGoogle = () => {
    const provider = new GoogleAuthProvider()
    signInWithPopup(auth, provider)
      .then(() => {
        console.log('Successfully registered with google!')
        router.push('/dashboard')
        isUserLogged.value = true
      })
      .catch((error) => {
        console.log(error.message)
      })
  }

  return {
    getCurrentUser,
    login,
    loginGoogle,
    register,
    registerGoogle,
    isUserLogged
  }
})
