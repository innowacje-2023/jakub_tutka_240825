import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import DashBoard from '../views/DashBoard.vue'
import LoginView from '../views/LoginView.vue'
import RegisterView from '../views/RegisterView.vue'
import PageNotFoundViewVue from '@/views/PageNotFoundView.vue'
import { useDatabaseLogic } from '@/stores/databaseLogic'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/dashboard',
      name: 'board',
      component: DashBoard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'page_not_found',
      component: PageNotFoundViewVue
    }
  ]
})

router.beforeEach(async (to, _from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const dbStore = useDatabaseLogic()
    if (await dbStore.getCurrentUser()) {
      next()
    } else {
      alert('Musisz być zalogowany, aby zobaczyć tę stronę!')
      next('/')
    }
  } else {
    next()
  }
})

export default router
